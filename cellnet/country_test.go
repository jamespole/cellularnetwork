package cellularnetwork_test

import (
	"testing"

	"bitbucket.org/jamespole/cellularnetwork"
)

func createCountry() *cellularnetwork.Country {
	country := cellularnetwork.Country{
		ID:   530,
		Name: "New Zealand",
	}
	return &country
}

func TestAddNetworkToCountry(t *testing.T) {
	country := createCountry()
	network := createNetwork()
	err := country.AddNetwork(network)
	if err != nil {
		t.Errorf("Error was non-nil, expected nil.")
	}
	err = country.AddNetwork(network)
	if err == nil {
		t.Errorf("Error was nil, expected non-nil.")
	}
}

func TestGetCountryByID(t *testing.T) {
	var countries cellularnetwork.Countries
	newZealand := createCountry()
	countries = append(countries, newZealand)
	countryFound := countries.GetByID(0)
	if countryFound != nil {
		t.Errorf("Country found was not-nil, expected nil.")
	}
	countryFound = countries.GetByID(530)
	if countryFound == nil {
		t.Errorf("Country found was nil, expected non-nil country")
	}
}
