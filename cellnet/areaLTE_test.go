package cellularnetwork_test

import (
	"testing"

	"bitbucket.org/jamespole/cellularnetwork"
)

func createAreaLTE() *cellularnetwork.AreaLTE {
	areaLTE := cellularnetwork.AreaLTE{
		ID:   41216,
		Name: "Auckland North",
	}
	return &areaLTE
}

func TestGetAreaLTEByID(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	a := createAreaLTE()
	a.Network = n
	var areaLTEs cellularnetwork.AreaLTEs
	areaLTEs = append(areaLTEs, a)
	foundAreaLTE := areaLTEs.GetByID(0, 0, 0)
	if foundAreaLTE != nil {
		t.Errorf("Error was non-nil, expected nil.")
	}
	foundAreaLTE = areaLTEs.GetByID(c.ID, n.ID, a.ID)
	if foundAreaLTE == nil {
		t.Errorf("Error was nil, expected non-nil.")
	}
}
