// Package cellularnetwork contains types and methods that can be used to
// describe a cellular (GSM, UMTS or LTE) network.
package cellularnetwork

import "fmt"

// Network describes a mobile network.
type Network struct {
	AreaLTEs  AreaLTEs  // TAC
	AreaUMTSs AreaUMTSs // URA
	CellLTEs  CellLTEs
	CellUMTSs CellUMTSs
	Country   *Country // MCC
	ID        int      // MNC
	Name      string
}

// Networks is a collection of pointers to networks.
type Networks []*Network

// AddAreaLTE adds the areaLTE to the network 'n', making sure to create
// associative links between areaLTE and 'n' (through areaLTE.network and
// n.AreaLTEs). Returns an error if there is an AreaLTE with the same ID
// associated with n.
func (n *Network) AddAreaLTE(areaLTE *AreaLTE) error {
	if n.AreaLTEs.GetByID(n.Country.ID, n.ID, areaLTE.ID) != nil {
		return fmt.Errorf("AreaLTE has an ID that is already associated with a AreaLTE within this network.")
	}
	areaLTE.Network = n
	n.AreaLTEs = append(n.AreaLTEs, areaLTE)
	return nil
}

// AddAreaUMTS adds the areaUMTS to the network 'n', making sure to create
// associative links between areaUMTS and 'n' (through areaUMTS.network and
// n.AreaUMTSs). Returns an error if there is an AreaUMTS with the same ID
// associated with n.
func (n *Network) AddAreaUMTS(areaUMTS *AreaUMTS) error {
	if n.AreaUMTSs.GetByID(n.Country.ID, n.ID, areaUMTS.ID) != nil {
		return fmt.Errorf("AreaUMTS with this ID already exists.")
	}
	areaUMTS.Network = n
	n.AreaUMTSs = append(n.AreaUMTSs, areaUMTS)
	return nil
}

// AddCellLTE adds the cellLTE to the network 'n', making sure to create
// associative links between cellLTE and 'n' (through cellLTE.network and
// n.CellLTEs). Returns an error if there is an CellLTE with the same ID
// associated with n.
func (n *Network) AddCellLTE(cellLTE *CellLTE) error {
	if n.CellLTEs.GetByID(n.Country.ID, n.ID, cellLTE.ID) != nil {
		return fmt.Errorf("CellLTE with this ID already exists.")
	}
	cellLTE.Network = n
	n.CellLTEs = append(n.CellLTEs, cellLTE)
	return nil
}

// AddCellUMTS adds the cellUMTS to the network 'n', making sure to create
// associative links between cellUMTS and 'n' (through cellUMTS.Network and
// n.CellUMTSs). Returns an error if there is a CellUMTS with the same ID
// associated with n.
func (n *Network) AddCellUMTS(cellUMTS *CellUMTS) error {
	if n.CellUMTSs.GetByID(n.Country.ID, n.ID, cellUMTS.ID) != nil {
		return fmt.Errorf("CellUMTS with this ID already exists.")
	}
	cellUMTS.Network = n
	n.CellUMTSs = append(n.CellUMTSs, cellUMTS)
	return nil
}

// GetByID returns the network with the given countryID and id within networks.
// Returns nil if a network with the given details could not be found.
func (networks Networks) GetByID(countryID int, id int) *Network {
	for _, network := range networks {
		if network.Country.ID == countryID && network.ID == id {
			return network
		}
	}
	return nil
}
