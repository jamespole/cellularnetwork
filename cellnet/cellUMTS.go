package cellularnetwork

// CellUMTS describes a UMTS cell within a UMTS network.
type CellUMTS struct {
	ID             int       // LCID
	Network        *Network  // MCC + MNC
	AreaUMTS       *AreaUMTS // URA
	UARFCN         int
	ScramblingCode int
}

// CellUMTSs is a collection of pointers to CellUMTSs.
type CellUMTSs []*CellUMTS

// GetByID returns the CellUMTS with the given country ID (as 'c'), network ID
// as ('n') and cell ID (as 'e') within cellUMTSs.
func (cellUMTSs CellUMTSs) GetByID(c int, n int, e int) *CellUMTS {
	for _, cellUMTS := range cellUMTSs {
		if cellUMTS.Network.Country.ID == c &&
			cellUMTS.Network.ID == n &&
			cellUMTS.ID == e {
			return cellUMTS
		}
	}
	return nil
}
