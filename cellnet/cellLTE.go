package cellularnetwork

// CellLTE describes a cell within a LTE network.
type CellLTE struct {
	ID        int // eNB-ID + CID
	Network   *Network
	AreaLTE   *AreaLTE
	EARFCN    int
	PCI       int // Physical Cell ID
	Bandwidth int
}

// CellLTEs is a collection of pointers to CellLTEs.
type CellLTEs []*CellLTE

// GetByID returns the CellLTE with the given country ID (as 'c'), network ID
// as ('n') and cell ID (as 'e') within cellLTes.
func (cellLTEs CellLTEs) GetByID(c int, n int, e int) *CellLTE {
	for _, cellLTE := range cellLTEs {
		if cellLTE.Network.Country.ID == c && cellLTE.Network.ID == n &&
			cellLTE.ID == e {
			return cellLTE
		}
	}
	return nil
}
