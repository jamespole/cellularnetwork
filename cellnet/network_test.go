package cellularnetwork_test

import (
	"testing"

	"bitbucket.org/jamespole/cellularnetwork"
)

func createNetwork() *cellularnetwork.Network {
	network := cellularnetwork.Network{
		ID:   1,
		Name: "Vodafone",
	}
	return &network
}

func TestAddAreaLTEToNetwork(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	a := createAreaLTE()
	n.AddAreaLTE(a)
	err := n.AddAreaLTE(a)
	if err == nil {
		t.Errorf("Return value was non-nil, expected non-nil.")
	}
}

func TestAddAreaUMTSToNetwork(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	a := createAreaUMTS()
	n.AddAreaUMTS(a)
	err := n.AddAreaUMTS(a)
	if err == nil {
		t.Errorf("Return value was nil, expected non-nil.")
	}
}

func TestAddCellLTEToNetwork(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	e := createCellLTE()
	n.AddCellLTE(e)
	err := n.AddCellLTE(e)
	if err == nil {
		t.Errorf("Return value was nil, expected non-nil.")
	}
}

func TestAddCellUMTSToNetwork(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	e := createCellUMTS()
	n.AddCellUMTS(e)
	err := n.AddCellUMTS(e)
	if err == nil {
		t.Errorf("Return value was nil, expected non-nil.")
	}
}

func TestGetNetworkById(t *testing.T) {
	country := createCountry()
	network := createNetwork()
	country.AddNetwork(network)
	var networks cellularnetwork.Networks
	networks = append(networks, network)
	networkFound := networks.GetByID(0, 0)
	if networkFound != nil {
		t.Errorf("Network found was not-nil, expected nil.")
	}
	networkFound = networks.GetByID(country.ID, network.ID)
	if networkFound == nil {
		t.Errorf("Network found was nil, expected non-nil network")
	}
}
