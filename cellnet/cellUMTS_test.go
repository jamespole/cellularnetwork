package cellularnetwork_test

import (
	"testing"

	"bitbucket.org/jamespole/cellularnetwork"
)

func createCellUMTS() *cellularnetwork.CellUMTS {
	e := cellularnetwork.CellUMTS{
		ID: 59106235,
	}
	return &e
}

func TestGetCellUMTSByID(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	e := createCellUMTS()
	e.Network = n
	var cellUMTSs cellularnetwork.CellUMTSs
	cellUMTSs = append(cellUMTSs, e)
	foundCellUMTS := cellUMTSs.GetByID(0, 0, 0)
	if foundCellUMTS != nil {
		t.Errorf("Error was non-nil, expected nil.")
	}
	foundCellUMTS = cellUMTSs.GetByID(c.ID, n.ID, e.ID)
	if foundCellUMTS == nil {
		t.Errorf("Error was nil, expected non-nil.")
	}
}
