package cellularnetwork_test

import (
	"testing"

	"bitbucket.org/jamespole/cellularnetwork"
)

func createAreaUMTS() *cellularnetwork.AreaUMTS {
	areaUMTS := cellularnetwork.AreaUMTS{
		ID:   1991,
		Name: "Upper North Island",
	}
	return &areaUMTS
}

func TestGetAreaUMTSByID(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	a := createAreaUMTS()
	a.Network = n
	var areaUMTSs cellularnetwork.AreaUMTSs
	areaUMTSs = append(areaUMTSs, a)
	foundAreaUMTS := areaUMTSs.GetByID(0, 0, 0)
	if foundAreaUMTS != nil {
		t.Errorf("Returned non-nil, expected nil.")
	}
	foundAreaUMTS = areaUMTSs.GetByID(c.ID, n.ID, a.ID)
	if foundAreaUMTS == nil {
		t.Errorf("Returned nil, expected nil.")
	}
}
