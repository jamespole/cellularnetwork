package cellularnetwork_test

import (
	"testing"

	"bitbucket.org/jamespole/cellularnetwork"
)

func createParentRegion() *cellularnetwork.Region {
	region := cellularnetwork.Region{
		ID:   1,
		Name: "World",
	}
	return &region
}

func createChildRegion() *cellularnetwork.Region {
	region := cellularnetwork.Region{
		ID:   2,
		Name: "New Zealand",
	}
	return &region
}

func TestAddChildToRegion(t *testing.T) {
	world := createParentRegion()
	newZealand := createChildRegion()
	err := world.AddChild(newZealand)
	if err != nil {
		t.Errorf("Error was non-nil, expected nil.")
	}
	err = world.AddChild(newZealand)
	if err == nil {
		t.Errorf("Error was nil, expected non-nil.")
	}
}

func TestGetRegionByID(t *testing.T) {
	var regions cellularnetwork.Regions
	world := createParentRegion()
	regions = append(regions, world)
	regionFound := regions.GetByID(0)
	if regionFound != nil {
		t.Errorf("Region found was non-nil, expected nil.")
	}
	regionFound = regions.GetByID(1)
	if regionFound == nil {
		t.Errorf("Region found was nil, expected non-nil.")
	} else if world.ID != regionFound.ID {
		t.Errorf("Region found (%v) not match expected region (%v).", *regionFound, world)
	}
}
