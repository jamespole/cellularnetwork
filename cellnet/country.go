package cellularnetwork

import "fmt"

// Country describes a country.
type Country struct {
	ID       int // MCC
	Name     string
	Networks Networks
}

// Countries is a collection of pointers to countries.
type Countries []*Country

// AddNetwork adds the network to the country, making sure to create associative
// links between network and country (through network.Country and
// country.Networks). Returns an error if there is an network with the same ID
// associated with country.
func (country *Country) AddNetwork(network *Network) error {
	if country.Networks.GetByID(country.ID, network.ID) != nil {
		return fmt.Errorf("Network has an ID that is already associated with a network within this country.")
	}
	network.Country = country
	country.Networks = append(country.Networks, network)
	return nil
}

// GetByID returns the country with the given id within countries. Returns nil
// if a country with the given id could not be found.
func (countries Countries) GetByID(id int) *Country {
	for _, country := range countries {
		if country.ID == id {
			return country
		}
	}
	return nil
}
