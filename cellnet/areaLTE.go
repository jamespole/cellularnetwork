package cellularnetwork

// AreaLTE describes a Tracking Area Code (TAC) within a LTE network.
type AreaLTE struct {
	ID      int
	Name    string
	Network *Network
}

// AreaLTEs is a collection of pointers to AreaLTEs.
type AreaLTEs []*AreaLTE

// GetByID returns the AreaLTE with the given countryID, networkID and id within
// areaLTEs. Returns nil if a AreaLTE with the given details could not be found.
func (areaLTEs AreaLTEs) GetByID(countryID int, networkID int,
	id int) *AreaLTE {
	for _, areaLTE := range areaLTEs {
		if areaLTE.Network.Country.ID == countryID &&
			areaLTE.Network.ID == networkID && areaLTE.ID == id {
			return areaLTE
		}
	}
	return nil
}
