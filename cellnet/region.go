package cellularnetwork

import "fmt"

// Region describes a region. A region typically refer to politically
// administrated region such as a country, a region or a city. A region can
// contain other regions (e.g. a country can contain several regions).
type Region struct {
	ID       int
	Name     string
	Parent   *Region
	Children Regions
}

// Regions is a collection of pointers to regions.
type Regions []*Region

// AddChild adds the child region to the parent region, making sure to create
// associative links between the two regions (through parent.Children and
// child.Parent). Returns an error if there is a child region with the same ID
// as the given child region.
func (parent *Region) AddChild(child *Region) error {
	if parent.Children.GetByID(child.ID) != nil {
		return fmt.Errorf("Child region has an ID that is already associated with a child region within this parent region.")
	}
	child.Parent = parent
	parent.Children = append(parent.Children, child)
	return nil
}

// GetByID returns a pointer to the region with the given id within regions.
// Returns nil if a region with the given id could not be found.
func (regions Regions) GetByID(id int) *Region {
	for _, region := range regions {
		if region.ID == id {
			return region
		}
	}
	return nil
}
