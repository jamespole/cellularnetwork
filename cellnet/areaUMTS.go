package cellularnetwork

// AreaUMTS describes a UTRAN Registration Area (URA) within a UMTS network.
// TODO: Confirm description above.
type AreaUMTS struct {
	ID      int // URA
	Name    string
	Network *Network // MCC + MNC
}

// AreaUMTSs is a collection of pointers to AreaUMTSs.
type AreaUMTSs []*AreaUMTS

// GetByID returns the AreaUMTS with the given country ID (as c), network ID (as
// n) and area id (as a). Returns nil if an AreaUMTS with the given details
// could not be found.
func (areaUMTSs AreaUMTSs) GetByID(c int, n int, a int) *AreaUMTS {
	for _, areaUMTS := range areaUMTSs {
		if areaUMTS.Network.Country.ID == c &&
			areaUMTS.Network.ID == n && areaUMTS.ID == a {
			return areaUMTS
		}
	}
	return nil
}
