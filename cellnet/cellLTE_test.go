package cellularnetwork_test

import (
	"testing"

	"bitbucket.org/jamespole/cellularnetwork"
)

func createCellLTE() *cellularnetwork.CellLTE {
	e := cellularnetwork.CellLTE{
		ID: 555777,
	}
	return &e
}

func TestGetCellLTEByID(t *testing.T) {
	c := createCountry()
	n := createNetwork()
	c.AddNetwork(n)
	e := createCellLTE()
	e.Network = n
	var cellLTEs cellularnetwork.CellLTEs
	cellLTEs = append(cellLTEs, e)
	foundCellLTE := cellLTEs.GetByID(0, 0, 0)
	if foundCellLTE != nil {
		t.Errorf("Error was non-nil, expected nil.")
	}
	foundCellLTE = cellLTEs.GetByID(c.ID, n.ID, e.ID)
	if foundCellLTE == nil {
		t.Errorf("Error was nil, expected non-nil.")
	}
}
